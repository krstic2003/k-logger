<?php
/**
* Plugin Name: Logger
* Description: Log plugin.
* Version: 1.0
* Author: Aleksandar Krstic
* Author URI: http://www.krstic.in.rs/
**/

if ( ! defined( 'ABSPATH' ) ){
	exit;
}

// Admin CSS
function klog_admin_style(){
        wp_register_style( 'klog_admin_css', plugin_dir_url( __FILE__ ) . 'css/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'klog_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'klog_admin_style' );

// Add admin menu item
add_action('admin_menu', 'klog_menu_item');
function klog_menu_item(){
    add_menu_page('Logger', 'Logger', 'administrator', 'klog_page', 'klog_settings', 'dashicons-list-view');

    //call register settings function
    add_action( 'admin_init', 'register_klog_settings' );
}

function register_klog_settings(){
	//register plugin settings
	register_setting( 'klog-settings-group', 'klog_path' );
}


function klog_settings(){
?>
<div class="wrap">
<h1>Logger</h1>

<form method="post" action="options.php">
    <?php 
    settings_fields( 'klog-settings-group' ); 
 	do_settings_sections( 'klog-settings-group' ); 

    ?>
    <div class="klog-table">
        <h2>Settings</h2>
        <div>
        	Log File Path (inside Uploads folder) - wp-content/uploads/<input type="text" class="klog-text" name="klog_path" value="<?php echo sanitize_option( 'klog_path', get_option('klog_path') ); ?>" />/klog.log
        	<div class="klog-notification">
        		- Example: If you enter "test", log file will be in - "wp-content/uploads/test/klog.log"<br>
        		- If you leave this field empty, log file will be in - "wp-content/uploads/klog.log"
        	</div>
        </div>  
    </div>
    
    <?php submit_button(); ?>

</form>
</div>
<?php 
}

// Log user data after Login
if (!function_exists('loginLog')){
	function loginLog($user_login, $user) { 
		global $wpdb;

		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$user_data = 'LOG IN :: Username: ' . $user->user_login . ' :: Roles: ' . implode(",", $user->roles) . ' :: IP: ' . $ip . ' :: Date-time: ' . date('Y-m-d h:i:s');
		
	    $custom_path = get_option('klog_path');
		if ($custom_path == '' || !isset($custom_path)) {
			$custom_path = '/';
		}else{
			if ( substr($custom_path,0,1) != '/' ) {
				$custom_path = '/' . $custom_path;
			}		

			if ( substr($custom_path,-1) != '/' ) {
				$custom_path = $custom_path . '/';
			}	
		}

		$final_path = wp_upload_dir()['basedir'] . $custom_path;

		if (!file_exists($final_path)) {
		    mkdir($final_path, 0777, true);
		}


	    $file = fopen($final_path .'klog.log','a'); 
	    echo fwrite($file,"\n" . $user_data); 
	    fclose($file); 
	}
}
add_action('wp_login', 'loginLog', 10, 2);

// Log user data after LogOut
if (!function_exists('logoutLog')){
	function logoutLog() { 
		$user = wp_get_current_user();

		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$user_data = 'LOG OUT :: Username: ' . $user->user_login . ' :: Roles: ' . implode(",", $user->roles) . ' :: IP: ' . $ip . ' :: Date-time: ' . date('Y-m-d h:i:s');


	    $custom_path = get_option('klog_path');
		if ($custom_path == '' || !isset($custom_path)) {
			$custom_path = '/';
		}else{
			if ( substr($custom_path,0,1) != '/' ) {
				$custom_path = '/' . $custom_path;
			}		

			if ( substr($custom_path,-1) != '/' ) {
				$custom_path = $custom_path . '/';
			}	
		}

		$final_path = wp_upload_dir()['basedir'] . $custom_path;

		//create directory id doesn't exist
		if (!file_exists($final_path)) {
		    mkdir($final_path, 0777, true);
		}

	    $file = fopen($final_path .'klog.log','a'); 
	    echo fwrite($file,"\n" . $user_data); 
	    fclose($file); 
	}
}
add_action('clear_auth_cookie', 'logoutLog', 10, 2);