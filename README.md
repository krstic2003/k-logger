=== K Logger ===

* Contributors: krstic2003
* Tags: log
* Requires PHP: 5.6
* Requires at least: 4.8
* Tested up to: 5.4.1
* Stable tag: 1.0
* License: GPLv2 or later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html

Simple User Activity log system.

== Description ==

K Logger is simple User Activity log system.

= Docs & Support =

/


= Privacy Notices =

With the default configuration, this plugin, in itself, does not:

* write any user personal data to the database;
* send any data to external servers;

With the default configuration, this plugin, in itself, use users IP address, and stores their data (username, role, IP address) to log file in file system.

= Required Plugins =

* No required plugins

= Translations =

* Translations are currently not supported.

== Installation ==

1. Upload the entire 'k-logger' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Logger' menu in your WordPress admin panel. There you can adjust plugin options.

== Changelog ==

= 1.0 =

* Initial Release